<?php

use App\Http\Controllers\AuthApiController;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthApiController::class, 'Login']);
Route::post('/registration', [AuthApiController::class, 'Registration']);

Auth::routes();
Route::get('/main', [HomeController::class, 'main']);
Route::get('/main/{id}', [HomeController::class, 'main']);
Route::post('/save_main', [HomeController::class, 'save_main']);
Route::post('/save_main/{id}', [HomeController::class, 'save_main']);
Route::post('/set_state/{id}', [HomeController::class, 'save_main']);

/*
Route::middleware('auth:api')->group(function(){
    Route::get('/user', [AuthApiController::class, "UserDetail"]);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/
