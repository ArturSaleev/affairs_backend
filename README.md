## Affairs Backend

##### Обозначения
$ = Запустить в консоле

### Необходимое ПО
- xampp (php, mysql or sqlite, apache)
- composer

### Установка
- Скачать файлы или подключиться к репозиторию проекта
- $ composer install
- $ composer start serve


## Описание
В данном проекте используется БД SQLITE.

Для работы с базой данных необходимо открыть файл php.ini и разкоментировать строчки

- extension=pdo_sqlite
- extension=sqlite3

И перезапустить apache (xampp)

 
Необходимо чтобы в папке database имелся файл базы данных affairs.sqlite.
Если данного файла нету тогда запустите комманды 
- $ php artisan new:db affairs
- $ php artisan migrate

