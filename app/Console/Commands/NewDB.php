<?php

namespace App\Console\Commands;

use App\Helpers\NewDatabaseHelpers;
use Illuminate\Console\Command;

class NewDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'new:db {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'База данных создана';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');
        $res = ((new NewDatabaseHelpers)->create($name));
        echo $res  ? "БД $name создана " : "Ошибка создания БД $name";
        return $res;
    }
}
