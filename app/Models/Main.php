<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $id_user
 * @property string $name
 * @property string $descr
 * @property string $date_begin
 * @property string $date_end
 * @property int $state
 * @property User $user
 */
class Main extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'main';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_user', 'name', 'descr', 'date_begin', 'date_end', 'state'];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user');
    }
}
