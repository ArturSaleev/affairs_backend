<?php


namespace App\Helpers;


class NewDatabaseHelpers
{
    public function create($name)
    {
        $path = public_path()."/../database/$name.sqlite";
        $res = new \SQLite3($path);
        return (!$res) ? false : true;
    }
}
