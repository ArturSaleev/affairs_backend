<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthApiController extends Controller
{

    public function Login(Request $request)
    {
        $login_credentials=[
            'email'=>$request->email,
            'password'=>$request->password,
        ];
        if(auth()->attempt($login_credentials)){
            $user_login_token= auth()->user()->createToken('PassportExample@Section.io')->accessToken;
            return $this->sendSucces(["user" => Auth::user(), 'token' => "Bearer $user_login_token"]);
        }
        else{
            return $this->sendError('Ошибка авторизации пользователя');
        }
    }

    public function Registration(Request $request)
    {
        $validate = $request->validate([
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:8',
        ]);

        $validate['password'] = bcrypt($request->password);

        $user = User::create($validate);

        $accessToken = $user->createToken('authToken')->accessToken;
        return $this->sendSucces(["user" => Auth::user(), 'token' => "Bearer $accessToken"]);
    }

    public function UserDetail()
    {
        return $this->sendSucces(['user' => auth()->user()]);
    }

}
