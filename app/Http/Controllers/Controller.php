<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendSucces($data = [], $message = "")
    {
        $result = [
            "success" => true,
            "message" => $message,
            "result" => $data
        ];
        return response()->json($result, 200);
    }

    public function sendError($msg, $state = 500)
    {
        $result = [
            "success" => false,
            "message" => $msg,
            "result" => []
        ];
        return response()->json($result, $state);
    }
}
