<?php

namespace App\Http\Controllers;

use App\Models\Main;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function main($id = 0)
    {
        if($id == 0){
            $q = Main::query()->where("id_user", Auth::id())->get();
        }else{
            $q = Main::find($id);
        }

        return $this->sendSucces($q);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function save_main(Request $request, $id = 0)
    {
        $id_user = Auth::id();
        $main = ($id == 0) ? new Main() : Main::find($id);
        $main->id_user = $id_user;
        $main->name = $request->post('name');
        $main->date_begin = date("Y-m-d H:i:s", strtotime($request->post('date_begin')));
        $main->date_end = date("Y-m-d H:i:s", strtotime($request->post('date_end')));
        if($request->has('descr')){
            $main->descr = $request->post('descr');
        }
        $main->save();

        return $this->main();
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function set_state(Request $request, $id)
    {
        $main = Main::find($id);
        $main->state = $request->post('state');
        $main->save();
        return $this->main();
    }
}
